require("cypress-xpath");


Cypress.Commands.add("getMemberOtp", () => {
    cy.fixture("auth/credentials").then((credentials) => {

        cy.request({
            method: 'POST',
            url: `${Cypress.env('api_host')}/public`,
            body: {
                phoneNo: `+1${credentials.phoneNumber}`
            },
            headers: {
                path: 'getMemberOtp',
            }

        }).then((response) => {
            cy.log("otp");
            cy.get('input[type="tel"]').first().type(response.body.data.Item.otp);
        })
    });
});
Cypress.Commands.add("clearTestMember", (jwt) => {
        cy.request({
            method: 'POST',
            url:`${Cypress.env('api_host')}/private`,
            //url:'http://localhost:4000/local/private ',
            body: {},
            headers: {
                path: 'clearTestMember',
                Authorization:jwt
            }

        }).then((response) => {
            cy.log(response);
        },err=>{
            cy.log('member clean error ',err)
        })
});

Cypress.Commands.add("loginFromHome", () => {
    cy.fixture("auth/credentials").then((credentials) => {
        cy.get('body').should('have.not.text', 'Modern Healthcare Starts Here').and('have.not.text', 'VISITOR PASSWORD').then(() => {
            cy.contains('Authentication Required').then(() => {
                cy.contains('label', 'VISITOR PASSWORD').find('[name="_vercel_password"]').type('GoGet123.321')
                cy.contains('button', "Log in").click()
            })
        })
        cy.get('[data-cy="login_button_container"]').contains('Login').click();
        cy.get('[data-cy="login_input"]').find('input').type(credentials.phoneNumber);
        cy.get('[data-cy="login_form_submit_button"]').click();
        cy.get('[data-cy="login_input"]').find('input');
        cy.log('waiting for OTP')
        cy.pause();
        cy.get('[data-cy="login_input"]').find('input').type(credentials.birthDate);
        cy.get('[data-cy="login_form_submit_button"]').click();
    });
});

Cypress.Commands.add("loginFromChat", () => {
    cy.fixture("auth/credentials").then((credentials) => {
        cy.get('#phone-number').type(credentials.phoneNumber)
        cy.contains('button', 'Next').click()
        cy.wait(1000);
        cy.getMemberOtp();
        cy.get('[name="USER_TEXT_INPUT"]').type(credentials.birthDate)
        cy.contains('button', 'Next').click()
    });
});
Cypress.Commands.add("loginFromChatNewUser", () => {
    cy.fixture("auth/credentials").then((credentials) => {
        cy.get('#phone-number').type(credentials.phoneNumber)
        cy.contains('button', 'Next').click()
        cy.wait(3000);
        cy.getMemberOtp();
        cy.get('[name="USER_TEXT_INPUT"]').type(credentials.newDOB)
        cy.contains('button', 'Next').click()
    });
});
Cypress.Commands.add('loginToVercel',(text)=>{
    cy.get('body').should('have.not.text', text).and('have.not.text', 'VISITOR PASSWORD').then((title)=>{
        if(title.text().includes('VISITOR PASSWORD')){
            cy.contains('Authentication Required').then(()=>{
                cy.contains('label','VISITOR PASSWORD').find('[name="_vercel_password"]').type('GoGet123.321')
                cy.contains('button',"Log in").click()
            })
        cy.log('no auth screen appeared');
        }
    })
})



