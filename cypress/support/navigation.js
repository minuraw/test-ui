require("cypress-xpath");

Cypress.Commands.add("visitGeneralVisitPage", () => {
  cy.visit("/", { failOnStatusCode: false });
  cy.vercelLogin();
  cy.get('[data-test="see_more_button"]', { multiple: true }).first().click();
  cy.visit("/", { failOnStatusCode: false });
  cy.vercelLogin();
  cy.get('[data-test="see_more_button"]', { multiple: true }).first().click();
  //cy.get('[data-cy="dynamic_utility_card_container"]');
  cy.get('[data-test="all_products_header"]').should("be.visible");
  cy.get('[data-test="clinical_card_continer"]')
    .contains("General Visit")
    .parent()
    .parent()
    .parent()
    .parent()
    .find("button")
    .click();
//   cy.get('[data-test="all_products_header"]').should("be.visible");
//   cy.get('[data-test="clinical_card_continer"]')
//     .contains("General Visit")
//     .parent()
//     .parent()
//     .parent()
//     .parent()
//     .find("button")
//     .click();
  //cy.contains('card','General Visit').parent().parent().find('button').click();
});

Cypress.Commands.add("vercelLogin", () => {
    cy.get('body').should('have.not.text', 'Modern Healthcare Starts Here').and('have.not.text', 'VISITOR PASSWORD').then($title => {
        if ($title.text().includes("VISITOR PASSWORD")) {
            cy.contains('label', 'VISITOR PASSWORD').find('[name="_vercel_password"]').type('GoGet123.321')
            cy.contains('button', "Log in").click()
        }
    })
});
Cypress.Commands.add("visitSymptomCheckerPage", () => {
    cy.visit('/', { failOnStatusCode: false });
    cy.vercelLogin()
    cy.contains('div', 'Symptom Checker').parent().parent().find('button').click()
});
Cypress.Commands.add("visitVaxYesPage", () => {
    cy.visit('/', { failOnStatusCode: false });
    cy.vercelLogin()
    cy.contains('div', 'VaxYes').parent().parent().parent().find('button').click();
});
Cypress.Commands.add("visitAcneAsyncPage", () => {
    cy.visit('/', { failOnStatusCode: false });
    cy.vercelLogin()
    cy.contains('div', 'Acne Prescription - Chat').parent().parent().parent().find('button').click();
});

Cypress.Commands.add("visitAcneSyncPage", () => {
    cy.visit('/', { failOnStatusCode: false });
    cy.vercelLogin()
    cy.contains('div', 'Acne Prescription - Video Visit').parent().parent().parent().find('button').click();
});
