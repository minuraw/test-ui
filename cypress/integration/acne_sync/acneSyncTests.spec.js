///<reference types="cypress"/>

import { fillSelection } from "../../utils/acne_syncUtils";

describe('Acne Chat Test Suite', () => {
    describe('should login through symptom checker', () => {
        it('test with short phone number', () => {
            cy.visitAcneSyncPage();
            cy.get('[name="USER_TEXT_INPUT"]').type('612')
            cy.contains('button', 'Next').click()
            cy.get('p').should('contain', 'Phone number is too short.')


        });
        it('test with invalid phone number', () => {
            cy.get('[name="USER_TEXT_INPUT"]').clear()
            cy.get('[name="USER_TEXT_INPUT"]').type('12121212121')
            cy.contains('button', 'Next').click()
            cy.get('p').should('contain', 'Phone number has an invalid country or area code.')
            cy.preserveAllCookies();

        });
        it('test with correct phone number', () => {
            cy.visitAcneSyncPage();
            cy.loginFromChat();
        })
    })
    describe('input address', () => {
        it('input address by typing', () => {
            cy.fixture("acne_sync/data").then((data) => {
                fillSelection.addressTyping(data.address_1.full_address);
                fillSelection.addressSubmit()
            })
        })
        it('input address by selecting', () => {
            fillSelection.updateAnswer()
            cy.fixture("acne_sync/data").then((data) => {
                cy.contains('button', "Can't find your address?").click()
                cy.wait(1000)
                fillSelection.addressFill(data.address_1);
                fillSelection.addressSubmit()
            })
        })
        it('Face Image Upload', () => {
            const filepath = 'images/face.jpeg'
            cy.get('input[type="file"]').attachFile(filepath);
            cy.get('button[type="submit"]').contains("Continue").click();
        })
        it('Id Image Upload', () => {
            const filepath = 'images/id.jpeg'
            cy.get('input[type="file"]').attachFile(filepath);
            cy.get('button[type="submit"]').contains("Continue").click();
        })
        it('gender selection input', () => {
            fillSelection.answerSelection("Male");
        })
        it('medical conditions inputs', () => {
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("Yes");
        })
        it('other medical conditions inputs', () => {
            fillSelection.customAnswerFill("test condition");
        })
        it('drug use inputs', () => {
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("No");
            fillSelection.answerSelection("No");
            fillSelection.answerSelection("No");
            fillSelection.answerSelection("No");
        })
        it('alcohol consume input', () => {
            fillSelection.answerSelection("Socially (1-2 per month)");
        })
        it('surgeries input', () => {
            fillSelection.answerSelection("No");
        })
        it('family history inputs', () => {
            fillSelection.answerSelection("Yes");
            fillSelection.answerSelection("No");
            fillSelection.answerSelection("No");
            fillSelection.answerSelection("No");
        })
        it('other dynamic attributes', () => {
            fillSelection.answerSelection("No - have my clinician help me decide")
            fillSelection.answerSelection("No")
            fillSelection.answerSelection("No")
            fillSelection.answerSelection("No")
            fillSelection.answerSelection("0-3 Months")
            fillSelection.answerSelection("No")
            fillSelection.answerSelection("No")
            fillSelection.answerSelection("20 to 100")
            fillSelection.answerSelection("Yes")
            fillSelection.answerSelection("Dry")
            fillSelection.answerSelection("Some Sensitivity")
        })
        it('Acne Image Upload', () => {
            const filepath = 'images/acne.jpeg'
            cy.get('input[type="file"]').attachFile(filepath);
            cy.get('button[type="submit"]').contains("Continue").click();
        })
        it('input pharmacy', () => {
            cy.fixture("acne_sync/data").then((data) => {
                fillSelection.pharmacyFill(data.pharmacy)
            });
        })
        it('input agreements',()=>{
            fillSelection.answerSelection("Agree to all")
        })

    })
});