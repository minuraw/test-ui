/// <reference types="cypress" />
require("cypress-xpath");
import { fillSelection } from "../../utils/generalVisitUtils";

var jwt;

describe("General Visi flow test suite", () => {
  before(() => {
    cy.visitGeneralVisitPage();
    cy.clearLocalStorageSnapshot();

  });
  describe("initiate", () => {
    beforeEach(() => {
      cy.restoreLocalStorage();
    });
    afterEach(() => {
      cy.saveLocalStorage();
      cy.preserveAllCookies();
    });
    it("intiater", () => {
      fillSelection.buttonSelection("I Agree");
      cy.loginFromChatNewUser();
      cy.fixture("auth/credentials").then((credentials) => {
        fillSelection.inputFill(credentials.newFirstname);
        cy.wait(1000);
        fillSelection.inputFill(credentials.newSecondname);
        cy.wait(6000);
      });
    });
    it("enter email", () => {
      cy.fixture("auth/credentials").then((credentials) => {
        fillSelection.inputFill(credentials.userEmail);
      });
    });
    it("input address by typing", () => {
      cy.fixture("generalVisit/data").then((data) => {
        fillSelection.addressTyping(data.address_1.full_address);
        fillSelection.addressSubmit();
      });   
    jwt =  localStorage.getItem(localStorage.key(1));
      cy.log('jwt', jwt)
    });
  });
  describe("flow inputs", () => {
    it("file upload", () => {
      const filepath = "images/face.jpeg";
      cy.get('input[type="file"]').attachFile(filepath);
      cy.get('button[type="submit"]').contains("Continue").click();
    });
    it("Id Image Upload", () => {
      const filepath = "images/id.jpeg";
      cy.get('input[type="file"]').attachFile(filepath);
      cy.get('button[type="submit"]').contains("Continue").click();
    });
    it("gender selection input", () => {
      fillSelection.buttonSelection("Male");
    });
    it("what am i going to dicuss", () => {
      fillSelection.inputFill("about fever");
    });
    it("medical conditions inputs", () => {
      for (let i = 0; i < 19; i++) {
        if(i === 1){
          fillSelection.buttonSelection("Yes");
        } else {
          fillSelection.buttonSelection("No");
        }
        cy.wait(500);
      }
    });
    it("medical conditions inputs", () => {
      for (let i = 0; i < 8; i++) {
          fillSelection.buttonSelection("No");
        cy.wait(500);
      }
    });
    it("flow inputs", () => {
      fillSelection.buttonSelection("No");
      fillSelection.buttonSelection("No");
      fillSelection.buttonSelection("No");
    });
    it("pharmacy input", () => {
      cy.get('[data-cy="chat_user_input_drawer"]')
        .find("input")
        .eq(1)
        .type("61220");
      cy.get('[data-cy="chat_user_input_drawer"]')
        .contains("button", "Next")
        .first()
        .click();
      cy.wait(3000);
      cy.get('[data-cy="chat_user_input_drawer"]').click("center");
      fillSelection.buttonSelection("Next");
    });
    it("agree to all", () => {
      fillSelection.buttonSelection("Agree To All");
    });
    it('clear member',()=>{
      cy.clearTestMember(jwt);
  })
  });
});
