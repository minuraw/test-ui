/// <reference types="cypress" />
import { fillSelection, localStorageAccess, qrCode } from "../../utils/vaxYesUtils";
var jwt;
var URL;

describe("User registration flow test suite", () => {
  before(() => {
    cy.clearLocalStorageSnapshot();
    cy.visitVaxYesPage();
  });
  afterEach(() => {
    cy.preserveAllCookies();
    cy.saveLocalStorage();
  });
  beforeEach(() => {
    cy.restoreLocalStorage();
  });
  describe("registration through vaxyes flow", () => {
    describe("Phone number input", () => {
      it("test with correct phone number", () => {
        cy.contains("button", "I Agree").click();
        cy.loginFromChatNewUser();
      });
    });
    describe("First name input", () => {
      it("entering first name ", () => {
        cy.fixture("auth/credentials").then((credentials) => {
          fillSelection.inputFill(credentials.newFirstname);
        });
      });
    });
    describe("second name input", () => {
      it("entering second name ", () => {
        cy.fixture("auth/credentials").then((credentials) => {
          fillSelection.inputFill(credentials.newSecondname);
        });
        // cy.get('[aria-label="refresh"]').click();
        // cy.contains("button", "I Agree").click();
      });
    });
    describe("get local storage", () => {
      it("get token from the local storage", () => {
        //member_id = decodeJWT.getMemberIdFromJWT(localStorageAccess.getJwtFromLocalStorage());
        jwt = localStorageAccess.getJwtFromLocalStorage();
      });
    });
    /*  describe("Login from chat", () => {
        it("chat login", () => {
          cy.loginFromChatNewUser();
        });
      });*/
    describe("Add vaxYes record", () => {
      beforeEach(() => {
        cy.restoreLocalStorage();
      });
      it("Add vaxYes record", () => {
        cy.wait(3000);
        cy.contains("button", "Add Vax Record").click();
      });
      it("vaccine card Image Upload", () => {
        const filepath = "images/card.jpeg";
        cy.get('input[type="file"]').attachFile(filepath);
        cy.get('button[type="submit"]').contains("Continue").click();
      });
    });

    describe("Double dose flow with Pfizer", () => {
      it("select Pfizer", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.buttonSelection(data.doubleDoseTypePfizer);
        });
      });
    });
    describe("first dose date input", () => {
      it("input valid first dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.firstDoseDate);
        });
      });
    });
    describe("first dose lot number", () => {
      it("input valid first dose lot number", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.firstDoseLotNumber);
        });
      });
    });
    describe("second dose date input", () => {
      it("input valid second dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.secondDoseDate);
        });
      });
    });
    describe("second dose lot number and booster dose", () => {
      it("booster dose", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.secondDoseLotNumber);
          fillSelection.buttonSelection("Yes");
          fillSelection.buttonSelection("Yes");
          fillSelection.buttonSelection("Moderna");
          fillSelection.inputFill(data.dateOfBoosterDose);
          fillSelection.inputFill(data.booosterDoseLotNumber);
        });
      });
    });
    describe("email input", () => {
      it("input valid email", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.emailAddress);
        });
      });
    });
    describe("Id image upload", () => {
      it("Id Image Upload", () => {
        const filepath = "images/id.jpeg";
        cy.get('input[type="file"]').attachFile(filepath);
        cy.get('button[type="submit"]').contains("Continue").click();
      });
    });
    describe("payment slider change", () => {
      it("payment slider", () => {
        fillSelection.doPaymentT();
      });
    });
    describe("view card", () => {
      beforeEach(() => {
        cy.restoreLocalStorage();
      });
      it("view card", () => {
        fillSelection.buttonSelection("View Card");
      });
      it("vercel login", () => {
        cy.vercelLogin();
        cy.pause();
        cy.reload();
        cy.vercelLogin();
      });
    });
  });
  describe("Update flow", () => {
    it("update intialize", () => {
      cy.contains("button", "Chats").click();
      fillSelection.buttonSelection("Update Certificate");
      cy.wait(5000);
      cy.vercelLogin()
      fillSelection.buttonSelection("Add Vax Record");
      cy.vercelLogin()
      const filepath = "images/card_2.jpg";
      cy.get('input[type="file"]').attachFile(filepath);
      cy.get('button[type="submit"]').contains("Continue").click();
      fillSelection.buttonSelection("Moderna");
      fillSelection.inputFill("09042021");
      fillSelection.inputFill("mrd234");
      fillSelection.inputFill("10042021");
      fillSelection.inputFill("mrd345");
    });
    it("booster dose", () => {
      fillSelection.buttonSelection("Yes");
      fillSelection.buttonSelection("Yes");
      fillSelection.buttonSelection("Moderna");
      fillSelection.inputFill("12152021");
      fillSelection.inputFill("mrd123");
      fillSelection.inputFill("updated@gogetdoc.com");
      fillSelection.buttonSelection("Yes");
      const filepath = "images/id_2.png";
      cy.get('input[type="file"]').attachFile(filepath);
      cy.get('button[type="submit"]').contains("Continue").click();
    });
    it("payment slider", () => {
      fillSelection.doPaymentT();
      fillSelection.buttonSelection("View Card");
      cy.reload();
      cy.vercelLogin();
    });
  });
  describe('validate the updated data',()=>{
    describe("dose 1 validation", () => {
        it("validate vaccine type", () => {
          cy.wait(5000);
          cy.reload();
          cy.vercelLogin();
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 1")
            .parent()
            .contains("VACCINE")
            .parent()
            .should("contain", "Moderna");
        });
        it("validate vaccine date", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 1")
            .parent()
            .contains("DATE")
            .parent()
            .should("contain", "Sep 04th, 2021");
        });
        it("validate vaccine Lot number", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 1")
            .parent()
            .contains("LOT NUMBER")
            .parent()
            .should("contain", "mrd234");
        });
      });
      describe("dose 2 validation", () => {
        it("validate vaccine type", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 2")
            .parent()
            .contains("VACCINE")
            .parent()
            .should("contain", "Moderna");
        });
        it("validate vaccine date", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 2")
            .parent()
            .contains("DATE")
            .parent()
            .should("contain", "Oct 04th, 2021");
        });
        it("validate vaccine Lot number", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 2")
            .parent()
            .contains("LOT NUMBER")
            .parent()
            .should("contain", "mrd345");
        });
      });
  })
  describe('bosster certificate updated data validation',()=>{
    it('vaildate vaccine type',()=>{
        cy.get('[data-cy="covid_card_dose_container"]').contains('div','Booster 1').parent().contains("VACCINE")
        .parent()
        .should("contain", "Moderna");
    })
    it('validate lot number',()=>{
        cy.get('[data-cy="covid_card_dose_container"]').contains('div','Booster 1').parent().contains("LOT NUMBER")
        .parent()
        .should("contain", "mrd123");
    })
    it('validate date',()=>{
        cy.get('[data-cy="covid_card_dose_container"]').contains('div','Booster 1').parent().contains("DATE")
        .parent()
        .should("contain", "Dec 15th, 2021");
    })
  })
  describe("Qr code validation", () => {
    it("get local storage", () => {
      cy.getLocalStorage("lastSearches").then(($lastSearches) => {
        cy.log($lastSearches);
      });
    });
    it("flip certificate", () => {
      cy.wait(10000)
      cy.reload();
      cy.vercelLogin();
      cy.get('[style="background-color: rgb(228, 237, 251);"]').then(() => {
        cy.get('[data-cy="covid_card_switch"]').first().click();
      });
      cy.wait(3000);
    })
    it('decode QR code',()=>{
    qrCode.validateQrCodeWihtoutDownload().then((url)=>{
      console.log("url from integrations", url);
      URL = url
    })
    cy.wait(500);
    })
  
  });
  describe('venue verfication',()=>{
    it('visit url',()=>{
      cy.visit(URL);
      cy.vercelLogin();
      cy.fixture("auth/credentials").then((data) => {
        cy.get('input').type(data.newDOB);
        cy.contains('button', 'Submit').click()
      });
    })
  })
  describe("clean up user data", () => {
    it("clear test member", () => {
      console.log("jwt token", jwt);
      cy.pause();
      cy.clearTestMember(jwt);
    });
  });
});
