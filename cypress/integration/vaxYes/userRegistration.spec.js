/// <reference types="cypress" />
import {
  fillSelection,
  localStorageAccess,
  qrCode,
} from "../../utils/vaxYesUtils";
var jwt;
var URL;

describe("User registration flow test suite", () => {
  afterEach(() => {
    cy.preserveAllCookies();
  });
  describe("registration through vaxyes flow", () => {
    before(() => {
      cy.clearLocalStorageSnapshot();
      cy.visitVaxYesPage();
    });
    beforeEach(() => {
      cy.restoreLocalStorage();
    });
    afterEach(() => {
      cy.preserveAllCookies();
      cy.saveLocalStorage();
    });
    describe("Phone number input", () => {
      it("test with correct phone number", () => {
        cy.contains("button", "I Agree").click();
        cy.loginFromChatNewUser();
      });
    });
    describe("First name input", () => {
      it("entering first name ", () => {
        cy.fixture("auth/credentials").then((credentials) => {
          fillSelection.inputFill(credentials.newFirstname);
        });
      });
    });
    describe("second name input", () => {
      it("entering second name ", () => {
        cy.fixture("auth/credentials").then((credentials) => {
          fillSelection.inputFill(credentials.newSecondname);
        });
        // cy.get('[aria-label="refresh"]').click();
        // cy.contains("button", "I Agree").click();
      });
    });
    describe("get local storage", () => {
      it("get token from the local storage", () => {
        //member_id = decodeJWT.getMemberIdFromJWT(localStorageAccess.getJwtFromLocalStorage());
        jwt = localStorageAccess.getJwtFromLocalStorage();
      });
    });
    /*  describe("Login from chat", () => {
      it("chat login", () => {
        cy.loginFromChatNewUser();
      });
    });*/
    describe("Add vaxYes record", () => {
      beforeEach(() => {
        cy.restoreLocalStorage();
      });
      it("Add vaxYes record", () => {
        cy.wait(3000);
        cy.contains("button", "Add Vax Record").click();
      });
      it("vaccine card Image Upload", () => {
        const filepath = "images/card.jpeg";
        cy.get('input[type="file"]').attachFile(filepath);
        cy.get('button[type="submit"]').contains("Continue").click();
      });
    });

    describe("Double dose flow with Pfizer", () => {
      it("select Pfizer", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.buttonSelection(data.doubleDoseTypePfizer);
        });
      });
    });
    describe("first dose date input", () => {
      it("input invalid first dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.invalidDate);
          cy.get("p").should("contain", data.invalidDateError);
        });
      });
      it("input incomplete first dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.incompleteDate);
          cy.get("p").should("contain", data.invalidDateError);
        });
      });
      it("input beyond first dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.beyondDate);
          cy.get("p").should("contain", data.beyondDateError);
        });
      });
      it("input valid first dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.firstDoseDate);
        });
      });
    });
    describe("first dose lot number", () => {
      it("input valid first dose lot number", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.firstDoseLotNumber);
        });
      });
    });
    describe("second dose date input", () => {
      it("input invalid second dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.invalidDate);
          cy.get("p").should("contain", data.invalidDateError);
        });
      });
      it("input incomplete second dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.incompleteDate);
          cy.get("p").should("contain", data.invalidDateError);
        });
      });
      it("input beyond second dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.beyondDate);
          cy.get("p").should("contain", data.beyondDateError);
        });
      });
      it("input date before first dose date for second dose", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.beforeDate);
          cy.get("p").should("contain", data.beforeDateError);
        });
      });
      it("input valid second dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.secondDoseDate);
        });
      });
    });
    describe("second dose lot number", () => {
      it("input valid second dose lot number", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.secondDoseLotNumber);
        });
      });
    });
    describe("booster dose", () => {
      it("input booster dose NO", () => {
        fillSelection.buttonSelection("No");
      });
    });
    describe("email input", () => {
      it("input invalid email", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.invalidEmailAddress);
          cy.get("p").should("contain", data.emailError);
        });
      });
      it("input valid email", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.emailAddress);
        });
      });
    });
    describe("Id image upload", () => {
      it("Id Image Upload", () => {
        const filepath = "images/id.jpeg";
        cy.get('input[type="file"]').attachFile(filepath);
        cy.get('button[type="submit"]').contains("Continue").click();
      });
    });
    describe("payment slider change", () => {
      it("payment slider", () => {
        fillSelection.doPaymentT();
      });
    });
    describe("Add booster", () => {
      it("add booster", () => {
        fillSelection.buttonSelection("Add Booster");
      });
    });
    describe("upload booster card image", () => {
      it("vaccine card Image Upload", () => {
        const filepath = "images/card.jpeg";
        cy.get('input[type="file"]').attachFile(filepath);
        cy.get('button[type="submit"]').contains("Continue").click();
      });
      it("select booster", () => {
        fillSelection.buttonSelection("Pfizer");
      });
      it("input valid booster dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.dateOfBoosterDose);
        });
      });
      it("input valid booster dose lot number", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.booosterDoseLotNumber);
        });
      });
    });
    describe("payment slider change", () => {
      before(() => {
        cy.restoreLocalStorage();
      });
      it("payment slider", () => {
        fillSelection.doPaymentT();
      });
      it("view card", () => {
        cy.wait(10000);
        fillSelection.buttonSelection("View Card");
        cy.vercelLogin();
      });
    });
    describe("Vaccine certificate validate", () => {
      before(() => {
        cy.restoreLocalStorage();
      });
      describe("dose 1 validation", () => {
        it("validate vaccine type", () => {
          cy.vercelLogin();
          cy.get('body').then($body => {
            if ($body.find('[style="background-color: rgb(228, 237, 251);"]').length > 0) {   
               cy.log('div exist')
            } else {
               cy.wait(10000);
               cy.reload();
               cy.vercelLogin()
            }
        });
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 1")
            .parent()
            .contains("VACCINE")
            .parent()
            .should("contain", "Pfizer-BioNTech");
        });
        it("validate vaccine date", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 1")
            .parent()
            .contains("DATE")
            .parent()
            .should("contain", "Sep 03rd, 2021");
        });
        it("validate vaccine Lot number", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 1")
            .parent()
            .contains("LOT NUMBER")
            .parent()
            .should("contain", "pfz123");
        });
      });
      describe("dose 2 validation", () => {
        it("validate vaccine type", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 2")
            .parent()
            .contains("VACCINE")
            .parent()
            .should("contain", "Pfizer-BioNTech");
        });
        it("validate vaccine date", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 2")
            .parent()
            .contains("DATE")
            .parent()
            .should("contain", "Oct 03rd, 2021");
        });
        it("validate vaccine Lot number", () => {
          cy.get('[style="background-color: rgb(228, 237, 251);"]')
            .contains("div", "Dose 2")
            .parent()
            .contains("LOT NUMBER")
            .parent()
            .should("contain", "fcn123");
        });
      });
      describe("booster certificate validation", () => {});
    });
    describe("Qr code validation", () => {
      it("get local storage", () => {
        cy.getLocalStorage("lastSearches").then(($lastSearches) => {
          cy.log($lastSearches);
        });
      });
      it("flip certificate", () => {
        cy.reload();
        cy.vercelLogin();
        cy.reload();
        cy.get('[style="background-color: rgb(228, 237, 251);"]').then(() => {
          cy.get('[data-cy="covid_card_switch"]').first().click();
        });
        cy.wait(3000);
      })
      it('decode QR code',()=>{
      qrCode.validateQrCodeWihtoutDownload().then((url)=>{
        console.log("url from integrations", url);
        URL = url
      })
      cy.wait(500);
      })
    
    });
    describe('venue verfication',()=>{
      it('visit url',()=>{
        cy.visit(URL);
        cy.vercelLogin();
        cy.fixture("auth/credentials").then((data) => {
          cy.get('input').type(data.newDOB);
          cy.contains('button', 'Submit').click()
        });
      })
    })
    describe("clean up user data", () => {
      it("clear test member", () => {
        console.log("jwt token", jwt);
        cy.clearTestMember(jwt);
      });
    });
  });
});
