/// <reference types="cypress" />
import { fillSelection, localStorageAccess,qrCode } from "../../utils/vaxYesUtils";
var jwt;
var URL;


describe("User registration flow test suite", () => {
  afterEach(() => {
    cy.preserveAllCookies();
  });
  describe("registration through vaxyes flow", () => {
    before(() => {
      cy.clearLocalStorageSnapshot();
      cy.visitVaxYesPage();
    });
    beforeEach(() => {
      cy.restoreLocalStorage();
    });
    afterEach(() => {
      cy.preserveAllCookies();
      cy.saveLocalStorage();
    });
    describe("Phone number input", () => {
      it("test with correct phone number", () => {
        cy.contains("button", "I Agree").click();
        cy.loginFromChatNewUser();
      });
    });
    describe("First name input", () => {
      it("entering first name ", () => {
        cy.fixture("auth/credentials").then((credentials) => {
          fillSelection.inputFill(credentials.newFirstname);
        });
      });
    });
    describe("second name input", () => {
      it("entering second name ", () => {
        cy.fixture("auth/credentials").then((credentials) => {
          fillSelection.inputFill(credentials.newSecondname);
        });
        // cy.get('[aria-label="refresh"]').click();
        // cy.contains("button", "I Agree").click();
      });
    });
    describe("get local storage", () => {
      it("get token from the local storage", () => {
        //member_id = decodeJWT.getMemberIdFromJWT(localStorageAccess.getJwtFromLocalStorage());
        jwt = localStorageAccess.getJwtFromLocalStorage();
      });
    });
    /*  describe("Login from chat", () => {
        it("chat login", () => {
          cy.loginFromChatNewUser();
        });
      });*/
    describe("Add vaxYes record", () => {
      beforeEach(() => {
        cy.restoreLocalStorage();
      });
      it("Add vaxYes record", () => {
        cy.wait(3000);
        cy.contains("button", "Add Vax Record").click();
      });
      it("vaccine card Image Upload", () => {
        const filepath = "images/card.jpeg";
        cy.get('input[type="file"]').attachFile(filepath);
        cy.get('button[type="submit"]').contains("Continue").click();
      });
    });

    describe("Double dose flow with Pfizer", () => {
      it("select Pfizer", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.buttonSelection(data.doubleDoseTypePfizer);
        });
      });
    });
    describe("first dose date input", () => {
      it("input valid first dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.firstDoseDate);
        });
      });
    });
    describe("first dose lot number", () => {
      it("input valid first dose lot number", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.firstDoseLotNumber);
        });
      });
    });
    describe("second dose date input", () => {
      it("input valid second dose date", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.secondDoseDate);
        });
      });
    });
    describe("second dose lot number", () => {
      it("input valid second dose lot number", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.secondDoseLotNumber);
        });
      });
    });
    describe("booster dose", () => {
      it("input booster dose NO", () => {
        fillSelection.buttonSelection("No");
      });
    });
    describe("email input", () => {
      it("input valid email", () => {
        cy.fixture("vaxyes/data").then((data) => {
          fillSelection.inputFill(data.emailAddress);
        });
      });
    });
    describe("Id image upload", () => {
      it("Id Image Upload", () => {
        const filepath = "images/id.jpeg";
        cy.get('input[type="file"]').attachFile(filepath);
        cy.get('button[type="submit"]').contains("Continue").click();
      });
    });
    describe("payment slider change", () => {
      it("payment slider", () => {
        fillSelection.doPaymentT();
      });
    });
    describe("view card", () => {
      before(() => {
        cy.restoreLocalStorage();
      });
      it("view card", () => {
        fillSelection.buttonSelection("View Card");
        cy.vercelLogin();
        //cy.loginToVercel("Covid-19 Vaccine Passport");
      });
    });
    describe("update booster certificate flow", () => {
        it("update booster certificate", () => {
          cy.vercelLogin();
          cy.contains("button", "Chats").click();
          cy.get('[data-cy="conversations_container"]')
            .contains("Covid-19 Vaccine Passport")
            .first()
            .click();
          fillSelection.buttonSelection("Add Booster");
        });
        it("image upload", () => {
          const filepath = "images/card_2.jpg";
          cy.get('input[type="file"]').attachFile(filepath);
          cy.get('button[type="submit"]').contains("Continue").click();
        });
        it("select moderna", () => {
          fillSelection.buttonSelection("Moderna");
        });

        it("update booster dose date fill", () => {
            cy.get('[data-cy="chat_user_input_drawer"]')
              .find("input")
              .clear()
              .type("12152021");
            cy.get('[data-cy="chat_user_input_drawer"]')
              .contains("button", "Next")
              .first()
              .click();
        });
    
        it("update booster dose lot number fill", () => {
            cy.get('[data-cy="chat_user_input_drawer"]')
              .find("input")
              .clear()
              .type('mrd123');
            cy.get('[data-cy="chat_user_input_drawer"]')
              .contains("button", "Next")
              .first()
              .click();
        });
      
        it("payment slider", () => {
          fillSelection.doPaymentT();
          cy.wait(10000);
        });

        it('View card',()=>{
            fillSelection.buttonSelection("View Card");
            cy.vercelLogin();
        })
        it('vaildate vaccine type',()=>{
            cy.get('[data-cy="covid_card_dose_container"]').contains('div','Booster 1').parent().contains("VACCINE")
            .parent()
            .should("contain", "Moderna");
        })
        it('validate lot number',()=>{
            cy.get('[data-cy="covid_card_dose_container"]').contains('div','Booster 1').parent().contains("LOT NUMBER")
            .parent()
            .should("contain", "mrd123");
        })
        it('validate date',()=>{
            cy.get('[data-cy="covid_card_dose_container"]').contains('div','Booster 1').parent().contains("DATE")
            .parent()
            .should("contain", "Dec 15th, 2021");
        })
    });
    describe("Qr code validation", () => {
      it("get local storage", () => {
        cy.getLocalStorage("lastSearches").then(($lastSearches) => {
          cy.log($lastSearches);
        });
      });
      it("flip certificate", () => {
        cy.wait(10000)
        cy.reload();
        cy.vercelLogin();
        cy.get('[style="background-color: rgb(228, 237, 251);"]').then(() => {
          cy.get('[data-cy="covid_card_switch"]').first().click();
        });
        cy.wait(3000);
      })
      it('decode QR code',()=>{
      qrCode.validateQrCodeWihtoutDownload().then((url)=>{
        console.log("url from integrations", url);
        URL = url
      })
      cy.wait(500);
      })
    
    });
    describe('venue verfication',()=>{
      it('visit url',()=>{
        cy.visit(URL);
        cy.vercelLogin();
        cy.fixture("auth/credentials").then((data) => {
          cy.get('input').type(data.newDOB);
          cy.contains('button', 'Submit').click()
        });
      })
    })

    describe("clean up user data", () => {
      it("clear test member", () => {
        console.log("jwt token", jwt);
        cy.clearTestMember(jwt);
      });
    });
  });
});
