/// <reference types="cypress" />
import { fillSelection } from "../../utils/vaxYesUtils";

describe("Covid-19 vaccine passport flow test suite", () => {
  describe('should login through vaxyes flow', () => {
    beforeEach(() => {
      cy.visitVaxYesPage()
    });
    afterEach(()=>{
      cy.preserveAllCookies();
    })
    it('test with short phone number', () => {
      cy.contains('button', 'I Agree').click()
      cy.get('[name="USER_TEXT_INPUT"]').type('612')
      cy.contains('button', 'Next').click()
      cy.get('p').should('contain', 'Phone number is too short.')

    });
    it('test with invalid phone number', () => {
      cy.contains('button', 'I Agree').click()
      cy.get('[name="USER_TEXT_INPUT"]').type('2121212121')
      cy.contains('button', 'Next').click()
      cy.get('p').should('contain', 'Phone number has an invalid country or area code.')

    });
    it('test with correct phone number', () => {
      cy.contains('button', 'I Agree').click()
      cy.loginFromChat();
      cy.vaxYesChatInit();
    })

  });
  describe("Double dose flow with Pfizer", () => {
    it("select Pfizer", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.buttonSelection(data.doubleDoseTypePfizer);
      });
    });
  });
  describe("first dose date input", () => {
    it("input invalid first dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.invalidDate);
        cy.get("p").should("contain", data.invalidDateError);
      });
    });
    it("input incomplete first dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.incompleteDate);
        cy.get("p").should("contain", data.invalidDateError);
      });
    });
    it("input beyond first dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.beyondDate);
        cy.get("p").should("contain", data.beyondDateError);
      });
    });
    it("input valid first dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.firstDoseDate);
      });
    });
  });
  describe("first dose lot number", () => {
    it("input valid first dose lot number", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.firstDoseLotNumber);
      });
    });
  });
  describe("second dose date input", () => {
    it("input invalid second dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.invalidDate);
        cy.get("p").should("contain", data.invalidDateError);
      });
    });
    it("input incomplete second dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.incompleteDate);
        cy.get("p").should("contain", data.invalidDateError);
      });
    });
    it("input beyond second dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.beyondDate);
        cy.get("p").should("contain", data.beyondDateError);
      });
    });
    it("input date before first dose date for second dose", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.beforeDate);
        cy.get("p").should("contain", data.beforeDateError);
      });
    });
    it("input valid second dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.secondDoseDate);
      });
    });
  });
  describe("second dose lot number", () => {
    it("input valid second dose lot number", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.secondDoseLotNumber);
      });
    });
  });

  describe("booster dose", () => {
    it("input booster dose yes", () => {
      fillSelection.buttonSelection("Yes");
    });
  });
  describe("booster dose brand", () => {
    it("input Pfizer as booster dose brand", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.buttonSelection(data.doubleDoseTypePfizer);
      });
    });
  });
  describe("booster dose date input", () => {
    it("input invalid second dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.invalidDate);
        cy.get("p").should("contain", data.invalidDateError);
      });
    });
    it("input incomplete booster dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.incompleteDate);
        cy.get("p").should("contain", data.invalidDateError);
      });
    });
    it("input beyond booster dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.beyondDate);
        cy.get("p").should("contain", data.beyondDateError);
      });
    });
    it("input date before first dose date for booster dose", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.beforeDate);
        cy.get("p").should("contain", data.beforeDateError);
      });
    });

    it("input date between first dose date and second dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.betweenDate);
        cy.get("p").should("contain", data.beforeDateError);
      });
    });

    it("input valid booster dose date", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.dateOfBoosterDose);
      });
    });
  });
  describe("booster dose lot number input", () => {
    it("input valid booster dose lot number", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.booosterDoseLotNumber);
      });
    });
  });
  describe("email input", () => {
    it("input invalid email", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.invalidEmailAddress);
        cy.get("p").should("contain", data.emailError);
      });
    });
    it("input valid email", () => {
      cy.fixture("vaxyes/data").then((data) => {
        fillSelection.inputFill(data.emailAddress);
      });
    });
  });
  describe("payment slider change", () => {
    it("payment slider", () => {
      fillSelection.doPaymentT();
    });
  });
});
