/// <reference types="cypress" />
import {
    localStorageAccess,
  } from "../../utils/vaxYesUtils";

var jwt;

describe('should login through vaxyes flow', () => {
    before(() => {
      cy.visitVaxYesPage()
      cy.clearLocalStorageSnapshot();
    });
    beforeEach(() => {
        cy.restoreLocalStorage();
      });
      afterEach(() => {
        cy.saveLocalStorage();
        cy.preserveAllCookies();
      });
    it('login', () => {
      cy.contains('button', 'I Agree').click()
      cy.loginFromChatNewUser();
    })
    it('entry',()=>{
      cy.contains('button','Chats',{timeout:10000}).click()
      cy.get('[data-cy="conversations_container"]').contains('Covid-19 Vaccine Passport').click();
    })
    it('get token',()=>{
      jwt =  localStorageAccess.getJwtFromLocalStorage();
      cy.log('jwt', jwt)
      
    })
    it('clear member',()=>{
        cy.clearTestMember(jwt);
    })

  });