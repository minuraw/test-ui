///<reference types="cypress"/>

import { fillSelection } from "../../utils/symptomBotUtils";
describe('SymptomChecker Test Suite', () => {

    describe('should login through symptom checker', () => {
        afterEach(() => {
            cy.preserveAllCookies();
        });
        it('test with short phone number', () => {
            cy.visitSymptomCheckerPage();
            cy.contains('button', 'I Agree').click()
            cy.get('[name="USER_TEXT_INPUT"]').type('612')
            cy.contains('button', 'Next').click()
            cy.get('p').should('contain', 'Phone number is too short.')
        });
        it('test with invalid phone number', () => {
            cy.visitSymptomCheckerPage();
            cy.contains('button', 'I Agree').click()
            cy.get('[name="USER_TEXT_INPUT"]').type('2121212121')
            cy.contains('button', 'Next').click()
            cy.get('p').should('contain', 'Phone number has an invalid country or area code.')

        });
        it('test with correct phone number', () => {
            cy.visitSymptomCheckerPage();
            cy.contains('button', 'I Agree').click()
            cy.loginFromChat();
        })
    })
    describe('input user attributes', () => {


        it('test gender Male input', () => {
            fillSelection.genderSelection("Male")

        })
        it('test other information input', () => {
            cy.contains('button', 'No').click()
            cy.contains('button', 'No').click()
            cy.contains('button', 'No').click()
            cy.contains('button', 'No').click()
        })
    })
    describe('input user symptoms', () => {
        it('test search symptoms', () => {
            fillSelection.searchSymptoms("Headache")
            fillSelection.searchSymptoms("Nausea")
            cy.contains('button', 'Next').click()
        })
        it('test dynamic symptoms', () => {
            cy.wait(2000)
            fillSelection.selectDynamicButton("Yes")
            fillSelection.selectDynamicButton("Yes")
            fillSelection.selectDynamicButton("Yes")
            fillSelection.selectDynamicButton("Yes")

        })

        it('should ask for contribution', () => {
            fillSelection.doPayment()
        })

        it('should show results', () => {
            cy.contains('Results').should('be.visible')
        })
    })
})