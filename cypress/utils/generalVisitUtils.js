export class generalvisitFills {
  addressTyping(address) {
    cy.get('[data-cy="schedule-screen-patient-address"]').type(address);
    cy.wait(1000);
    cy.get('[data-cy="schedule-screen-patient-address"]').click("center");
  }
  addressFill(address) {
    cy.get('[data-cy="input-address-country"]').select(address.country);
    cy.get('[data-cy="input-street"]').type(address.street);
    cy.get('[data-cy="input-city"]').type(address.city);
    cy.get('[data-cy="input-address-state"]').select(address.state);
    cy.get('[data-cy="input-zip_code"]').type(address.zip);
    cy.get('[data-cy="input-line2"]').type(address.additional);
    cy.get('[data-cy="input-line3"]').type(address.unit_no);
  }
  addressSubmit() {
    cy.get('[data-cy="schedule-screen-patient-address"]')
      .contains("Next")
      .click();
  }
  updateAnswer() {
    cy.get('[data-cy="update_answer_container"]').find("a").click();
  }
  buttonSelection(selection) {
    cy.get('[data-cy="chat_user_input_drawer"]')
      .contains('button:visible',selection)
      .click();
  }
  inputFill(input) {
    cy.wait(500);
    cy.get('[data-cy="chat_user_input_drawer"]')
      .find("input")
      .clear()
      .type(input);
    cy.get('[data-cy="chat_user_input_drawer"]')
      .contains("button", "Next")
      .first()
      .click();
  }
  answerSelection(answer) {
    cy.contains("button", answer).click();
  }
}

export const fillSelection = new generalvisitFills();
