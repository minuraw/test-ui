export class SymptombotFills{
    genderSelection(gender){
        cy.contains('button',gender).click()
    }
    searchSymptoms(symptom){
        cy.get('[placeholder="Search symptoms, e.g. headache"]').type(symptom)
        cy.contains('div',symptom).parent().click()
    }
    selectDynamicButton(answer){
        cy.get(`[data-cy="dynamic_input_button"]`).contains(answer).then($button=>{
            if($button.is(':visible')){
                cy.get(`[data-cy="dynamic_input_button"]`).contains(answer).click()
                
            }
        })
        cy.wait(1000)
    }
    doPayment(){
        cy.get('[data-cy="payment_slider"]').find('[role="slider"]').should('exist').then(()=>{
            cy.get('[data-cy="payment_slider"]').find('[role="slider"]').click()
            cy.get('[data-cy="payment_slider"]').find('[role="slider"]').type(
                "{leftarrow}{leftarrow}{leftarrow}{leftarrow}{leftarrow}"
            )
            cy.contains('button','Next').click()
        }) 
    }
}

export const fillSelection=new SymptombotFills()