/// <reference types="cypress" />
import { Decoder } from '@nuintun/qrcode';
import jwt_decode from "jwt-decode";


export class VaxYesFills {
  buttonSelection(selection) {
    cy.get('[data-cy="chat_user_input_drawer"]')
      .find("button")
      .contains(selection)
      .click();
  }
  inputFill(input) {
    cy.wait(500);
    cy.get('[data-cy="chat_user_input_drawer"]')
      .find("input")
      .clear()
      .type(input);
    cy.get('[data-cy="chat_user_input_drawer"]')
      .contains("button", "Next")
      .first()
      .click();
  }
  doPayment() {
    cy.get('[data-cy="payment_slider"]')
      .find('[role="slider"]')
      .should("exist")
      .then(() => {
        cy.get('[data-cy="payment_slider"]').find('[role="slider"]').click();
        cy.get('[data-cy="payment_slider"]')
          .find('[role="slider"]')
          .type("{leftarrow}{leftarrow}{leftarrow}{leftarrow}{leftarrow}");
        // cy.contains('button','Next').click()
      });
  }
  doPaymentT() {
    cy.get('[data-cy="payment_slider"]')
      .find('[role="slider"]')
      .should("exist")
      .then(() => {
        cy.get('[data-cy="payment_slider"]').find('[role="slider"]').click();
        cy.get('[data-cy="payment_slider"]')
          .find('[role="slider"]')
          .type("{leftarrow}{leftarrow}{leftarrow}{leftarrow}{leftarrow}");
        cy.contains("button", "Next").click();
      });
  }
}

export class QrCode{
  validateQrCode(image) {
    const qrcode = new Decoder();
    qrcode.scan("data:image/png;base64," + image).then((decodedQr) => {
      console.log("Decoded qr code:", decodedQr);
      expect(decodedQr).to.have.property("data");
      expect(decodedQr.data).to.contain(
        "https://member.gogetdoc.com/vaxyes/venue-verification"
      );
    });
  }
  validateQrCodeWihtoutDownload(){
      const qrcode = new Decoder();
      let pathToImg = "";
      let url = '';
      return new Cypress.Promise((resolve,reject)=>{
        cy.get('img[data-cy="covid-card_image"]')
        .first()
        .screenshot('qrcode', {
          onAfterScreenshot($el, props) {
            console.log("image source", $el.prop("src"));
            pathToImg = props.path;
          }
        }).then(() => {
          cy.readFile(pathToImg, 'base64').then((str) => {
            qrcode.scan('data:image/png;base64,' + str).then(decodedQr => {
              console.log('Decoded qr code:', decodedQr);
              expect(decodedQr).to.have.property('data')
              expect(decodedQr.data).to.contain('https://member.gogetdoc.com/vaxyes/venue-verification')
              resolve(decodedQr.data)
            });
          })
        })
      })
    }
  
}

export class LocalStorageAccess{
  getJwtFromLocalStorage(){
    var values = [],
              keys = Object.keys(window.localStorage),
              i = keys.length;
      
          while ( i-- ) {
              values.push( window.localStorage.getItem(keys[i]) );
          }
          console.log(values[1]);
          return values[1];
  }
}

export class DecodeJWT{
  getMemberIdFromJWT(token){
    var decoded = jwt_decode(token);
    console.log(decoded);
    return decoded.member_id;
  }
}

export const fillSelection = new VaxYesFills();
export const localStorageAccess = new LocalStorageAccess();
export const decodeJWT = new DecodeJWT();
export const qrCode = new QrCode();
