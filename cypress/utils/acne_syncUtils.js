export class AcneChatFills{
    addressTyping(address){
        cy.get('[data-cy="schedule-screen-patient-address"]').type(address);
        cy.wait(3000)
        cy.get('[data-cy="schedule-screen-patient-address"]').type("{uparrow}{enter}");

    }
    addressFill(address){
        cy.get('[data-cy="input-address-country"]').select(address.country);
        cy.get('[data-cy="input-street"]').type(address.street);
        cy.get('[data-cy="input-city"]').type(address.city);
        cy.get('[data-cy="input-address-state"]').select(address.state);
        cy.get('[data-cy="input-zip_code"]').type(address.zip);
        cy.get('[data-cy="input-line2"]').type(address.additional);
        cy.get('[data-cy="input-line3"]').type(address.unit_no);
    }
    addressSubmit(){
        cy.get('[data-cy="schedule-screen-patient-address"]').find('form').submit();
    }
    updateAnswer(){
        cy.get('[data-cy="update_answer_container"]').find('a').click()
    }
    answerSelection(answer){
        cy.contains('button',answer).click();
    }
    customAnswerFill(answer){
        cy.get('input[name="USER_TEXT_INPUT"]').type(answer);
        cy.contains('button', 'Next').click()
    }
    pharmacyFill(pharmacy){
        cy.get('input[name="name"]').type(pharmacy.name)
        cy.contains('button','Search').click()
        cy.contains('button','Set as my pharmacy').first().click()
    }
    

}

export const fillSelection=new AcneChatFills()